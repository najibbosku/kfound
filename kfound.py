#!/usr/bin/env python3
import os
import asyncio
from telegram import Bot

async def send_message_to_telegram(bot_token, chat_id, message):
    bot = Bot(token=bot_token)
    await bot.send_message(chat_id=chat_id, text=message)

async def send_file_contents_to_telegram(file_path, bot_token, chat_id):
    if os.path.isfile(file_path):
        with open(file_path, 'r') as file:
            file_contents = file.read()

        await send_message_to_telegram(bot_token, chat_id, file_contents)

async def send_link_to_telegram(link, bot_token, chat_id):
    await send_message_to_telegram(bot_token, chat_id, link)


bot_token = 'CHAT_TOKEN_ID'
chat_id = 'CHAT_ID'
file_path = '/PATH/TO/FILE/KEYFOUNDKEYFOUND.txt'
link = 'https://tenor.com/view/bitcoin-gif-11349536'

async def main():
    if os.path.isfile(file_path):
        await send_file_contents_to_telegram(file_path, bot_token, chat_id)
        await send_link_to_telegram(link, bot_token, chat_id)
    else:
        print("File does not exist.")

loop = asyncio.get_event_loop()
loop.run_until_complete(main())

